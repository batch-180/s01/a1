package com.zuitt;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scannerMaangas = new Scanner(System.in);

        System.out.println("Enter your first name: ");

        String fName = scannerMaangas.next();

        System.out.println("Enter your last name: ");

        String lName = scannerMaangas.next();

        System.out.println("Enter your 1st subject grade: ");

        double grade1 = scannerMaangas.nextDouble();

        System.out.println("Enter your 2nd subject grade: ");

        double grade2 = scannerMaangas.nextDouble();

        System.out.println("Enter your 3rd subject grade: ");

        double grade3 = scannerMaangas.nextDouble();

        double totalGrade = grade1 + grade2 + grade3;

        double gradeAve = totalGrade / 3;


        System.out.println("Good Day, " + fName + " " + lName +".");
        System.out.println("Your grade average is: " + gradeAve);


    }

}
